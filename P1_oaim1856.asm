;Nev: Orosz Arnold-Daniel
;Azonosito: oaim1856
;Csoport: 513

;Labor: Projekt
;Feladat: 1

;Feladat szovege:Mandelbrot fraktál generálás

;				 20 pont – sebesség optimalizálás SSE vektorizálással (egyszerre 4 pixel kiszámolása), min. 1024x768 felbontás, true color (24/32 bit), mozgatás, zoom, 
;				 szín animáció billentyűzettel/egérrel

; Compile:
; nasm -f win32 P1_oaim1856.asm
; nlink P1_oaim1856.obj -lgfx -o P1_oaim1856.exe
; P1_oaim1856.exe

%include 'gfx.inc'

global main

section .text
	
main:
	
	;megnyitunk egy 1024x768- as felbontasu ablakot
	;eax, ebx regiszterekben van a felbontas erteke
	;ecx regiszterben a megnyitas modja(fullscreen(1)/windows(0)
	;edx regiszterben a megnyitott ablak cimet tartalmazo memoriacim
	mov		eax,1024
	mov		ebx,768
	mov		ecx,0
	mov		edx,ablak
	call	gfx_init
	;esi,edi regisztereket felhasznaljuk majd a mozgatas/zoomolashoz
	xor		esi, esi	
	xor		edi, edi
	;xmm7 regiszterbe berakjuk az 1-et, amit a zoomolasnal hasznalunk
	movss	xmm7,[egy]
	
.mainloop:
	
	;megkezdjuk a kirajztolast, eax megkapja a framebuffer cimet
	call	gfx_map
	xor		ecx,ecx

.y:
	
	;vegigmegyunk a pixeleken
	cmp		ecx,768
	jge		.yvege
	xor		edx,edx

.x:
	
	;vegigmegyunk a pixeleken
	cmp		edx,1024
	jge		.xvege
	;4 pixelt szamolunk egyszerre, inicializaljuk oket feketere, vagyis minden pixel 3 szinet(R,G,B) lenullazzuk
	mov		[eax],word 0
	mov		[eax+1],word 0
	mov		[eax+2],word 0
	mov		[eax+4],word 0
	mov		[eax+5],word 0
	mov		[eax+6],word 0
	mov		[eax+8],word 0
	mov		[eax+9],word 0
	mov		[eax+10],word 0
	mov		[eax+12],word 0
	mov		[eax+13],word 0
	mov		[eax+14],word 0
	;megnezzuk hogy az adott 4 pixel kozul melyik van benne a mandelbrot halmazban
	jmp		.halmaz
	
.tovabb:

	;tovabbmegyunk 4 pixellel, az eax erteket 16-al noveljuk mert minden pixelhez 4 ertek tartozik
	add		eax,16
	add		edx,4
	jmp		.x
	
.xvege:

	inc		ecx
	jmp		.y
	
;vizsgaljuk 4 pixelrol, hogy benne van-e a halmazban
.halmaz:

	push	edx
	push	ecx
	
	;alap zoomolasi ertek(a pixeleket atformalja descartes koordinata rendszerbe azaltal, hogy elossza oket a megadott ertekkel)
	mov		ebx,300
	
	;kulon kiszamoljuk az eltolas erteket, annak erdekeben hogy a zoom a kepernyo kozepe fele menjen
	cvtsi2ss	xmm0,ebx
	mov			ebx,[offsetx]
	cvtsi2ss  	xmm1,ebx
	divss		xmm1,xmm0
	;elosszuk az alap ertekkel, majd betesszuk 4 cimre a 4 pixelhez
	movss		[eltx],xmm1
	movss		[eltx+4],xmm1
	movss		[eltx+8],xmm1
	movss		[eltx+12],xmm1
	
	;ugyanugy y eseteben, mint x eseteben a fentiek alapjan
	mov			ebx,[offsety]
	cvtsi2ss	xmm1,ebx
	divss		xmm1,xmm0
	movss		[elty],xmm1
	movss		[elty+4],xmm1
	movss		[elty+8],xmm1
	movss		[elty+12],xmm1
	
	;alap eltolasi ertek, szinten a koordinatarendszer kialakitasahoz, illetve a zoom tartalmazza az adott zoomolas merteket
	mov		ebx,[zoom]
	sub		ecx,384
	sub		edx,512

	;letrehozzuk a 4 pixel szamat, kiindulva az eredeti ertektol, noveljuk 1-el, majd berakjuk az x egyik cimere, es igy tovabb
	cvtsi2ss	xmm0,edx
	
	movss		[x],xmm0
	inc			edx
	cvtsi2ss	xmm0,edx
	movss		[x+4],xmm0
	inc			edx
	cvtsi2ss	xmm0,edx
	movss		[x+8],xmm0
	inc			edx
	cvtsi2ss	xmm0,edx
	movss		[x+12],xmm0
	
	;letrehozzuk a 4 pixel szamat, kiindulva az eredeti ertektol, noveljuk 1-el, majd berakjuk az y egyik cimere, es igy tovabb
	cvtsi2ss	xmm0,ecx
	movss		[y],xmm0
	movss		[y+4],xmm0
	movss		[y+8],xmm0
	movss		[y+12],xmm0
	
	;az adott zoom erteket is megnegyszerezzuk a 4 pixel kiszamitasahoz
	cvtsi2ss	xmm0,ebx
	movss		[z],xmm0
	movss		[z+4],xmm0
	movss		[z+8],xmm0
	movss		[z+12],xmm0
	
	;visszaallitjuk a pixelek koordinatait
	pop			ecx
	pop			edx
	
	;lementjuk az x, y es a zoom 4 erteket xmm regiszterekbe, a 4-szeres szamitas erdekeben
	movups	xmm0,[z]
	movups	xmm1,[x]
	movups	xmm2,[y]
	
	;kiszamoljuk a zoom-ot elosztva az x es y koordinatakat a zoom ertekevel
	divps	xmm1,xmm0
	divps	xmm2,xmm0
	
	;ezt kovetoen hozzaadjuk az eltolasokat x-hez es y-hoz 
	movups	xmm3,[eltx]
	addps	xmm1,xmm3
	movups	xmm3,[elty]
	addps	xmm2,xmm3
	
	;inicializalunk nehany xmm regisztert a kovetkezo szamitasokhoz
	xorps	xmm3,xmm3
	xorps	xmm4,xmm4
	xorps	xmm5,xmm5
	xorps	xmm6,xmm6
	
	;szamoljuk a lepeseket
	mov		[szamol],dword 0
	
;implementaltuk a mandelbrot kepletet(z → z² + c) ahol a c a jelenlegi pixel koordinataibol alkotott komplex szam
;ha egy adott pixel nincs benne a mandelbrot halmazban akkor vegrehajtja ezt a muveletet a maximalis lepesszamnak megfeleloen, anelkul hogy a z gyokenek erteke meghaladna a 2-ot
;ha viszont nincs benne, akkor valamelyik lepesnel megszakad, mivel a z gyoke meghaladta a 2-ot, es annak fuggvenyeben, hogy mikor szinezzuk majd a pixelt
.ciklus:
	
	;a szamol memoriacimre berakjuk a kivant maximalis lepesszamot
	cmp		[szamol],dword 60
	je		.tovabb
	
	;itt szamoljuk az elozo komplex szam negyzetet
	;a komplex szam alakja a+bi, ahol xmm3 az a-t, xmm4 a b-t jelenti
	;a^2
	movups		xmm3,xmm5
	mulps		xmm3,xmm5
	
	;b^2
	movups		xmm4,xmm6
	mulps		xmm4,xmm6
	
	;a^2-b^2
	subps		xmm3,xmm4
	
	;a*b
	movups		xmm4,xmm5
	mulps		xmm4,xmm6
	;2*a*b
	addps		xmm4,xmm4
	
	;hozzaadjuk a pixel koordinataibol alkotott szamot
	addps		xmm3,xmm1
	addps		xmm4,xmm2
	
	;lementjuk az uj kapott szamot, amivel majd ismeteljuk a muveletet
	movups		xmm5,xmm3
	movups		xmm6,xmm4
	
	;kiszamoljuk a z gyoket, de a gyokvonast elhagyjuk, igy 2 helyett 4 el hasonlitjuk majd
	mulps		xmm3,xmm3
	mulps		xmm4,xmm4
	addps		xmm3,xmm4
	;berakjuk a 4 et az xmm4 es regiszterbe
	mov			ebx,4
	cvtsi2ss	xmm4,ebx
	
	;vesszuk a 4 kiszamolt erteket egyenkent
	movups	[p],xmm3
	movss	xmm0,[p]
	comiss	xmm0,xmm4
	jb		.tovabb2
	;ha egyszer sem haladta meg a 4-et, akkor fekete marad, ha viszont meghaladta, szinezzuk
	;en itt 3 fele osztottam a szinezest, mivel 75 a maxlepes szama ezert 0-25 kozott a pixel elso szinet modositottam(R), ha 25-50, akkor a masodikat(G), ha 50-75, akkor a hamradikat(B)
	;ezekhez hozzaadtam a lepesszamnak(amikor meghaladtak a 4-et) egy bizonyos szamszorosat, ugy hogy 0-255 koze essen, akkor ha meg nem volt hozzaadva semmi
	xor		ebx,ebx
	mov		ebx,[szamol]
	cmp		ebx,20
	jg		.masszin
	cmp		ebx,40
	jg		.masszin2
	;ha 0-25 kozott van, 10 szereset adtam hozza
	cmp		[eax],word 0
	jne		.masszin
	add		ebx,dword [szin]
	imul	ebx,10
	mov		[eax],bl
	jmp		.tovabb2
	
.masszin:

	;ha 25-50 kozott van, 5 szoroset adtam hozza
	cmp		[eax+1],word 0
	jne		.masszin2
	add		ebx,dword [szin]
	imul 	ebx,5
	mov		[eax+1],bl
	jmp		.tovabb2

.masszin2:

	;ha 50-75 kozott van, 3 szorosat adtam hozza
	cmp		[eax+2],word 0
	jne		.tovabb2
	add		ebx,dword [szin]
	imul 	ebx,3
	mov		[eax+2],bl
	
;mindezekhez hozzajon a szin memoriacimen levo ertek, ami arra szolgal, hogy gombnyomasra valtozzanak a szinek, azaltal, hogy a szorzas elott meg hozzaad egy bizonyos erteket
;a kovetkezo esetekben ugyanezt csinaltam, csak a kovetkezo 3 pixellel
	
.tovabb2:

	movss	xmm0,[p+4]
	comiss	xmm0,xmm4
	jb		.tovabb3
	xor		ebx,ebx
	mov		ebx,[szamol]
	cmp		ebx,20
	jg		.masszin3
	cmp		ebx,40
	jg		.masszin4
	cmp		[eax+4],word 0
	jne		.masszin3
	add		ebx,dword [szin]
	imul	ebx,10
	mov		[eax+4],bl
	jmp		.tovabb3
	
.masszin3:

	cmp		[eax+5],word 0
	jne		.masszin4
	add		ebx,dword [szin]
	imul 	ebx,5
	mov		[eax+5],bl
	jmp		.tovabb3

.masszin4:

	cmp		[eax+6],word 0
	jne		.tovabb3
	add		ebx,dword [szin]
	imul 	ebx,3
	mov		[eax+6],bl
	
.tovabb3:
	
	movss	xmm0,[p+8]
	comiss	xmm0,xmm4
	jb		.tovabb4
	xor		ebx,ebx
	mov		ebx,[szamol]
	cmp		ebx,20
	jg		.masszin5
	cmp		ebx,40
	jg		.masszin6
	cmp		[eax+8],word 0
	jne		.masszin5
	add		ebx,dword [szin]
	imul	ebx,10
	mov		[eax+8],bl
	jmp		.tovabb4
	
.masszin5:

	cmp		[eax+9],word 0
	jne		.masszin6
	add		ebx,dword [szin]
	imul 	ebx,5
	mov		[eax+9],bl
	jmp		.tovabb4

.masszin6:
	
	cmp		[eax+10],word 0
	jne		.tovabb4
	add		ebx,dword [szin]
	imul 	ebx,3
	mov		[eax+10],bl
	
.tovabb4:

	movss	xmm0,[p+12]
	comiss	xmm0,xmm4
	jb		.tovabb5
	xor		ebx,ebx
	mov		ebx,[szamol]
	cmp		ebx,20
	jg		.masszin7
	cmp		ebx,40
	jg		.masszin8
	cmp		[eax+12],word 0
	jne		.masszin7
	add		ebx,dword [szin]
	imul	ebx,10
	mov		[eax+12],bl
	jmp		.tovabb5
	
.masszin7:

	cmp		[eax+13],word 0
	jne		.masszin8
	add		ebx,dword [szin]
	imul 	ebx,5
	mov		[eax+13],bl
	jmp		.tovabb5

.masszin8:

	cmp		[eax+14],word 0
	jne		.tovabb5
	add		ebx,dword [szin]
	imul 	ebx,3
	mov		[eax+14],bl
	
.tovabb5:
	
	inc		dword [szamol]
	jmp		.ciklus

;ha vegeztunk mind az 1024*768 pixel ellenorzesevel, kirajzoljuk oket a megadott szinezesek alapjan	
.yvege:

	;felszabaditjuk a framebuffert
	call	gfx_unmap
	call	gfx_draw

	;zoom, mozgatas billentyuzetrol
	xor		ebx, ebx			
	mov		ecx,[nmov]	;mozgatas merteke
	mov		edx,[pmov]
	
.eventloop:

	;kerunk egy beavatkozast a billenytuzetrol
	call	gfx_getevent
	
	;zoom(zoom in(i), zoom out (o))
	;ha az i megvolt nyomva, xmm7 be berakunk egy szorzot ami a zoom merteket szorozza addig, amig fel nem engedjuk a billentyut
	cmp		eax,'i'
	jne		.nemi
	movss	xmm7,[szorzo]
	jmp		.update
	
.nemi:
	
	;ha felengedtuk az xmm7 1-et kap
	cmp		eax,-'i'
	jne		.nemni
	movss	xmm7,[egy]
	jmp		.update
	
.nemni:
	
	;ha az o megvolt nyomva, xmm7 be berakunk egy osztot ami a zoom merteket szorozza addig, amig fel nem engedjuk a billentyut
	cmp		eax,'o'
	jne		.nemo
	movss	xmm7,[oszto]
	jmp		.update
	
.nemo:
	
	;ha felengedtuk az xmm7 1-et kap
	cmp		eax,-'o'
	jne		.nemno
	movss	xmm7,[egy]
	jmp		.update
	
.nemno:
	
	;ha az c megvolt nyomva, a szin1 memoriacimre berakunk egy 1-est ami majd a szinezest fogja modositani azaltal, hogy meg az ertekhez ez is hozzaadodik
	cmp		eax,'c'
	jne		.nemc
	mov		[szin1],dword 1
	jmp		.update
	
.nemc:

	;ha felengedtuk, a szin1 0-at kap
	cmp		eax,-'c'
	jne		.nemnc
	mov		[szin1],ebx
	jmp		.update
	
.nemnc:
	
	cmp		eax, 'w'	;w megvolt nyomva
	cmove	edi, ecx	;eltoljuk -25 el
	cmp		eax, -'w'	;w felvolt engedve
	cmove	edi, ebx	;nem csinal semmit (0)
	cmp		eax, 's'	;s megvolt nyomva
	cmove	edi, edx	;eltoljuk 25 el
	cmp		eax, -'s'	;s felvolt engedve
	cmove	edi, ebx	;nem csinal semmit (0)
	cmp		eax, 'a'	;a megvolt nyomva
	cmove	esi, ecx	;eltoljuk -25 el
	cmp		eax, -'a'	;a felvolt engedve
	cmove	esi, ebx	;nem csinal semmit (0)
	cmp		eax, 'd'	;d megvolt nyomva
	cmove	esi, edx	;eltoljuk 25 el
	cmp		eax, -'d'	;d felvolt engedve
	cmove	esi, ebx	;nem csinal semmit (0)
	
	;kilepes
	cmp		eax, 23			;az ablak X-e megvolt nyomva
	je		.end
	cmp		eax, 27			;ha a bilentyuzet ESC-je megvolt nyomva
	je		.end
    
;frissitjuk a beavatkozasokat
.update:

	;az eltolasokat hozzaadjuk egy memoriacimhez, ami majd a kirajzolasnal hozzaadodik a pixel koordinataihoz
	add		[offsetx], esi
	add		[offsety], edi
	
	;beszorozzuk a zoom2-ot az xmm7-el
	mov		eax,[zoom2]
	cvtsi2ss	xmm1,eax
	mulss	xmm1,xmm7
	cvtss2si	eax,xmm1
	mov		[zoom],eax
	
	;az eredeti zoom2-ot is modositjuk, igy a zoom folyamatos lesz
	mov		[zoom2],eax
	
	;hozzaadjuk a szin-hez a szin2-n levo erteket
	mov		eax,[szin1]
	add		[szin],eax

	;ha befele zoomoltunk, osztjuk a pmow, nmow ertekeit, csokkentve a mozgatas merteket
	movss	xmm0,[szorzo]
	comiss	xmm7,xmm0
	jne		.mainloop
	mov		eax,[pmov]
	cvtsi2ss	xmm0,eax
	movss	xmm1,[szorzo2]
	divss	xmm0,xmm1
	cvtss2si	eax,xmm0
	mov		[pmov],eax
	mov		eax,[nmov]
	cvtsi2ss	xmm0,eax
	divss	xmm0,xmm1
	cvtss2si	eax,xmm0
	mov		[nmov],eax
	
	;ujrarajzoljuk az abrat
	jmp		.mainloop
	
.end:

	;ha megnyomtuk az x-et vagy az esc-et, kilepunk a letrehozott ablakbol
	call	gfx_destroy
    ret

section .data

	a dd 0
	b dd 0
	ablak db "Mandelbrot fraktal",0
	offsetx dd 0
	offsety dd 0
	zoom dd 300
	zoom2 dd 300
	szamol dd 0
	szorzo dd 1.1
	oszto dd 0.9
	szorzo2 dd 1.99
	szin dd 0
	szin1 dd 0
	egy dd 1.0
	pmov dd 25
	nmov dd -25
	
section .bss

	szam resb 255
	y resd 256
	x resd 256
	z resd 256
	p resd 256
	eltx resd 256
	elty resd 256